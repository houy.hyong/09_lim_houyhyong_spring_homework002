--Create database
CREATE DATABASE houyhyong_spring_homework02;

-- Create table CUSTOMER _ customer_tb
CREATE TABLE IF NOT EXISTS customer_tb(
    customer_id SERIAL PRIMARY KEY,
    customer_name VARCHAR(50),
    customer_address VARCHAR(255),
    customer_phone VARCHAR(15)
);

-- Create table PRODUCT _ product_tb
CREATE TABLE IF NOT EXISTS product_tb(
    product_id SERIAL PRIMARY KEY,
    product_name VARCHAR(50) NOT NULL,
    product_price NUMERIC(5, 2) NOT NULL
);

-- Create table INVOICE _ invoice_tb
CREATE TABLE IF NOT EXISTS invoice_tb(
    invoice_id SERIAL PRIMARY KEY,
    invoice_date TIMESTAMP,
    customer_id INT REFERENCES customer_tb(customer_id)
);

-- Create table INVOICE x DETAIL _ invoice_detail_tb
CREATE TABLE IF NOT EXISTS invoice_detail_tb(
    id SERIAL PRIMARY KEY,
    invoice_id INT REFERENCES invoice_tb(invoice_id) ON UPDATE CASCADE ON DELETE CASCADE,
    product_id INT REFERENCES product_tb(product_id) ON UPDATE CASCADE ON DELETE CASCADE
);




