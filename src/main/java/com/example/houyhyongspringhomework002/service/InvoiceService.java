package com.example.houyhyongspringhomework002.service;

import com.example.houyhyongspringhomework002.model.entity.Invoice;
import com.example.houyhyongspringhomework002.model.request.InvoiceRequest;
import com.example.houyhyongspringhomework002.model.request.ProductRequest;
import com.example.houyhyongspringhomework002.repository.InvoiceRepository;


import java.util.List;

public interface InvoiceService {

    // 1 - Get all invoice
    List<Invoice> getAllInvoice();

    // 2 - Get invoice by ID
    Invoice getInvoiceById(Integer invoiceId);

    // 3 - Add new invoice
    Integer addNewInvoice(InvoiceRequest invoiceRequest);


    // 4 - Update invoice by id
    Integer updateInvoiceById(InvoiceRequest invoiceRequest, Integer invoiceId);


    // 5 - Delete invoice by id
    boolean deleteInvoiceById(Integer invoiceId);


}
