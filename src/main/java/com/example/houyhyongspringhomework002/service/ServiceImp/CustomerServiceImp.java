package com.example.houyhyongspringhomework002.service.ServiceImp;

import com.example.houyhyongspringhomework002.model.entity.Customer;
import com.example.houyhyongspringhomework002.model.request.CustomerRequest;
import com.example.houyhyongspringhomework002.repository.CustomerRepository;
import com.example.houyhyongspringhomework002.service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CustomerServiceImp implements CustomerService {

    //inject Customer Repository
    private final CustomerRepository customerRepository;
    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    // 1- Get All Customers
    @Override
    public List<Customer> getAllCustomer() {
        return customerRepository.findAllCustomer();
    }


    // 2- Get Customer by ID
    @Override
    public Customer getCustomerById(Integer customerId) {
        return customerRepository.getAllCustomerById(customerId);
    }


    // 3- Add new customer
    @Override
    public Integer addNewCustomer(CustomerRequest customerRequest) {
        Integer customerId = customerRepository.addCustomer(customerRequest);
        return customerId;
    }


    // 4- Update customer by id
    @Override
    public Integer updateCustomer(CustomerRequest customerRequest, Integer customerId) {
        Integer customerUpdate = customerRepository.updateCustomer(customerRequest, customerId);
        return customerUpdate;
    }


    // 5- Delete customer by id
    @Override
    public boolean deleteCustomerById(Integer customerId) {
        return customerRepository.deleteCustomerById(customerId);
    }
}
