package com.example.houyhyongspringhomework002.service.ServiceImp;

import com.example.houyhyongspringhomework002.model.entity.Product;
import com.example.houyhyongspringhomework002.model.request.ProductRequest;
import com.example.houyhyongspringhomework002.repository.ProductRepository;
import com.example.houyhyongspringhomework002.service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {

    //inject Product Repository
    private final ProductRepository productRepository;
    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }


    // 1- Get All Products
    @Override
    public List<Product> getAllProduct() {
        return productRepository.findAllProduct();
    }


    // 2- Get product by ID
    @Override
    public Product getProductById(Integer productId) {
        return productRepository.getAllProductById(productId);
    }


    // 3- Add new product
    @Override
    public Integer addNewProduct(ProductRequest productRequest) {
        Integer productId = productRepository.addProduct(productRequest);
        return productId;
    }


    // 4- Update product by ID
    @Override
    public Integer updateProduct(ProductRequest productRequest, Integer productId) {
        Integer productUpdate = productRepository.updateProduct(productRequest, productId);
        return productUpdate;
    }


    // 5 - Delete product by ID
    @Override
    public boolean deleteProductById(Integer productId) {
        return productRepository.deleteProductById(productId);
    }

}
