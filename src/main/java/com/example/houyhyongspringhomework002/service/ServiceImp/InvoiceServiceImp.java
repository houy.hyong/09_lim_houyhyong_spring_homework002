package com.example.houyhyongspringhomework002.service.ServiceImp;

import com.example.houyhyongspringhomework002.model.entity.Invoice;
import com.example.houyhyongspringhomework002.model.request.InvoiceRequest;
import com.example.houyhyongspringhomework002.repository.InvoiceRepository;
import com.example.houyhyongspringhomework002.service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {
    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    // 1- Get all invoice
    @Override
    public List<Invoice> getAllInvoice() {
        return invoiceRepository.findAllInvoice();
    }


    // 2 - Get invoice by ID
    @Override
    public Invoice getInvoiceById(Integer invoiceId) {
        return invoiceRepository.findInvoiceById(invoiceId);
    }


    // 3 - Add new invoice
    @Override
    public Integer addNewInvoice(InvoiceRequest invoiceRequest) {
        //need variable for store invoiceId
        Integer storeInvoiceId = invoiceRepository.addInvoice(invoiceRequest);

        // list product -> need loop
        for(Integer productId : invoiceRequest.getProductId()){
            //add into invoice_detail
            invoiceRepository.saveProductByInvoiceId(storeInvoiceId, productId);
        }
        return storeInvoiceId;
    }


    // 4 - Update invoice by id
    @Override
    public Integer updateInvoiceById(InvoiceRequest invoiceRequest, Integer invoiceId) {
        //need variable for store invoiceId
        Integer storeInvoiceId = invoiceRepository.updateInvoice(invoiceRequest,invoiceId);

        invoiceRepository.deleteIdtByInvoiceId(invoiceId);

        for(Integer invoice : invoiceRequest.getProductId()){
            invoiceRepository.saveProductByInvoiceId(storeInvoiceId, invoice);
        }

        return storeInvoiceId;
    }


    // 5 - Delete invoice by id
    @Override
    public boolean deleteInvoiceById(Integer invoiceId) {
        return invoiceRepository.deleteInvoiceId(invoiceId);
    }
}
