package com.example.houyhyongspringhomework002.service;

import com.example.houyhyongspringhomework002.model.entity.Product;
import com.example.houyhyongspringhomework002.model.request.CustomerRequest;
import com.example.houyhyongspringhomework002.model.request.ProductRequest;

import java.util.List;

public interface ProductService {

    // 1- Get All Products
    List<Product> getAllProduct();


    // 2- Get product by ID
    Product getProductById(Integer productId);

    // 3- Add new product
    Integer addNewProduct(ProductRequest productRequest);


    // 4- Update product by ID
    Integer updateProduct(ProductRequest productRequest, Integer productId);


    // 5 - Delete product by ID
    boolean deleteProductById(Integer productId);


}
