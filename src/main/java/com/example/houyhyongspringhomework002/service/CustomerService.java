package com.example.houyhyongspringhomework002.service;

import com.example.houyhyongspringhomework002.model.entity.Customer;
import com.example.houyhyongspringhomework002.model.request.CustomerRequest;

import java.util.List;

public interface CustomerService {
    // 1- Get All Customers
    List<Customer> getAllCustomer();

    // 2- Get Customer by ID
    Customer getCustomerById(Integer customerId);

    // 3- Add new customer
    Integer addNewCustomer(CustomerRequest customerRequest);


    // 4- Update customer by id
    Integer updateCustomer(CustomerRequest customerRequest, Integer customerId);


    // 5- Delete customer by id
    boolean deleteCustomerById(Integer customerId);

}
