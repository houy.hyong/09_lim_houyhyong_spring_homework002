package com.example.houyhyongspringhomework002.controller;

import com.example.houyhyongspringhomework002.model.entity.Customer;
import com.example.houyhyongspringhomework002.model.entity.Product;
import com.example.houyhyongspringhomework002.model.request.CustomerRequest;
import com.example.houyhyongspringhomework002.model.request.ProductRequest;
import com.example.houyhyongspringhomework002.model.response.CustomerResponse;
import com.example.houyhyongspringhomework002.model.response.ProductResponse;
import com.example.houyhyongspringhomework002.service.ProductService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    //inject Product Service
    private final ProductService productService;
    public ProductController(ProductService productService) {
        this.productService = productService;
    }


    // 1- Get All Products
    @GetMapping("/get-all-product")
    public ResponseEntity<ProductResponse<List<Product>>> getAllProduct(){
        ProductResponse<List<Product>> response = ProductResponse.<List<Product>>builder()
                .payload(productService.getAllProduct())
                .message("Fetch all products is successful !!")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }


    // 2- Get product by ID
    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<ProductResponse<Product>> getProductById(@PathVariable("id") Integer productId){
        ProductResponse<Product> response = null;
        if(productService.getProductById(productId) != null){
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(productId))
                    .message("This product was successfully found !!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }



    // 3- Add new product
    @PostMapping("/add-new-product")
    public ResponseEntity<ProductResponse<Product>> addNewProduct(@RequestBody ProductRequest productRequest){
        Integer productId = productService.addNewProduct(productRequest);

        if(productId != null){
            ProductResponse<Product> response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(productId))
                    .message("Add new product is successful !!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }



    // 4- Update product by ID
    @PutMapping("/update-product-by-id/{id}")
    public ResponseEntity<ProductResponse<Product>> updateProductById(
            @RequestBody ProductRequest productRequest,
            @PathVariable("id") Integer productId)
    {
        ProductResponse<Product> response = null;
        Integer productUpdateId = productService.updateProduct(productRequest, productId);

        if(productUpdateId != null){
            response = ProductResponse.<Product>builder()
                    .payload(productService.getProductById(productUpdateId))
                    .message("Product successfully updated !!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }



    // 5 - Delete product by ID
    @DeleteMapping("delete-product-by-id/{id}")
    public ResponseEntity<ProductResponse<String>> deleteProductById(@PathVariable("id") Integer productId){
        ProductResponse<String> response = null;
        if(productService.deleteProductById(productId) == true){
            response = ProductResponse.<String>builder()
                    .message("Successfully deleted product !!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }

}
