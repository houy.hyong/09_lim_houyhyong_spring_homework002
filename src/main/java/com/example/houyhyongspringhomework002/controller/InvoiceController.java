package com.example.houyhyongspringhomework002.controller;

import com.example.houyhyongspringhomework002.model.entity.Customer;
import com.example.houyhyongspringhomework002.model.entity.Invoice;
import com.example.houyhyongspringhomework002.model.request.CustomerRequest;
import com.example.houyhyongspringhomework002.model.request.InvoiceRequest;
import com.example.houyhyongspringhomework002.model.response.CustomerResponse;
import com.example.houyhyongspringhomework002.model.response.InvoiceResponse;
import com.example.houyhyongspringhomework002.model.response.ProductResponse;
import com.example.houyhyongspringhomework002.service.InvoiceService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {

    // inject
    private final InvoiceService invoiceService;
    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }

    // 1 - get all invoice
    @GetMapping("/get-all-invoice")
    public ResponseEntity<InvoiceResponse<List<Invoice>>> getAllInvoice(){
        InvoiceResponse<List<Invoice>> response = InvoiceResponse.<List<Invoice>>builder()
                .payload(invoiceService.getAllInvoice())
                .message("Fetch all invoice is successfully !!")
                .success(true)
                .build();
        return ResponseEntity.ok(response);
    }

    // 2 - Get invoice by ID
    @GetMapping("get-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<Invoice>> getInvoiceById(@PathVariable("id") Integer invoiceId){
        InvoiceResponse<Invoice> response = null;
        if (invoiceService.getInvoiceById(invoiceId) != null ){
            response = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(invoiceId))
                    .message("This invoice was found !!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }


    // 3 - Add new invoice
    @PostMapping("/add-new-invoice")
    public ResponseEntity<InvoiceResponse<Invoice>> addNewInvoice(@RequestBody InvoiceRequest invoiceRequest){
        InvoiceResponse<Invoice> response= null;

        Integer storeInvoiceId = invoiceService.addNewInvoice(invoiceRequest);

        if (storeInvoiceId != null){
            response = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(storeInvoiceId))
                    .message("Insert new invoice is successfully !!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        return null;
    }

    // 4 - Update invoice by id
    @PutMapping("update-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<Invoice>> updateInvoiceById(
            @RequestBody InvoiceRequest invoiceRequest,
            @PathVariable("id") Integer invoiceId)
    {

        InvoiceResponse<Invoice> response = null;
        Integer invoiceUpdateId = invoiceService.updateInvoiceById(invoiceRequest,invoiceId);

        if(invoiceUpdateId != null){
            response = InvoiceResponse.<Invoice>builder()
                    .payload(invoiceService.getInvoiceById(invoiceUpdateId))
                    .message("Update successfully")
                    .success(true)
                    .build();

            return ResponseEntity.ok(response);
        }
        else{
            return ResponseEntity.notFound().build();
        }

    }


    // 5 - Delete invoice by id
    @DeleteMapping("delete-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<String>> deleteInvoiceById(@PathVariable("id") Integer invoiceId){
        InvoiceResponse<String> response = null;

        if(invoiceService.deleteInvoiceById(invoiceId) == true){
            response = InvoiceResponse.<String>builder()
                    .message("Successfully deleted invoice !!")
                    .success(true)
                    .build();

            return ResponseEntity.ok(response);
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }


}
