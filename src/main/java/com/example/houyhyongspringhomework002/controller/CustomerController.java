package com.example.houyhyongspringhomework002.controller;


import com.example.houyhyongspringhomework002.model.entity.Customer;
import com.example.houyhyongspringhomework002.model.request.CustomerRequest;
import com.example.houyhyongspringhomework002.model.response.CustomerResponse;
import com.example.houyhyongspringhomework002.service.CustomerService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }


    // 1- Get all customers
    @GetMapping("/get-all-customer")
    public ResponseEntity<CustomerResponse<List<Customer>>> getAllCustomer(){

        CustomerResponse<List<Customer>> response = CustomerResponse.<List<Customer>>builder()
                .payload(customerService.getAllCustomer())
                .message("Fetch all customer is successfully !!")
                .success(true)
                .build();

        return ResponseEntity.ok(response);
    }


    // 2- Get Customer by ID
    @GetMapping("/get-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<Customer>> getCustomerById(@PathVariable("id") Integer customerId){

        CustomerResponse<Customer> response = null;

        if(customerService.getCustomerById(customerId) != null){
            response = CustomerResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(customerId))
                    .message("This customer was found !!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else{
//            response = CustomerResponse.<Customer>builder()
//                    .message("This customer was not found !!")
//                    .success(false)
//                    .build();
            return ResponseEntity.notFound().build();
        }
    }


    // 3- Add new customer
    @PostMapping("/add-new-customer")
    public ResponseEntity<CustomerResponse<Customer>> addNewCustomer(@RequestBody CustomerRequest customerRequest){
        Integer storeCustomerId = customerService.addNewCustomer(customerRequest);

        if(storeCustomerId != null){
            CustomerResponse<Customer> response = CustomerResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(storeCustomerId))
                    .message("Insert customer is successfully !!")
                    .success(true)
                    .build();

            return ResponseEntity.ok(response);
        }
        return null;
    }


    // 4- Update customer by id
    @PutMapping("update-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<Customer>> updateCustomerById(
            @RequestBody CustomerRequest customerRequest,
            @PathVariable("id") Integer customerId)
    {

        CustomerResponse<Customer> response = null;
        Integer customerUpdateId = customerService.updateCustomer(customerRequest, customerId);

        if(customerUpdateId != null){
            response = CustomerResponse.<Customer>builder()
                    .payload(customerService.getCustomerById(customerUpdateId))
                    .message("Customer successfully updated !!")
                    .success(true)
                    .build();

            return ResponseEntity.ok(response);
        }
        else{
            return ResponseEntity.notFound().build();
        }

    }



    // 5- Delete customer by id
    @DeleteMapping("delete-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<String>> deleteCustomerById(@PathVariable("id") Integer customerId){

        CustomerResponse<String> response = null;
        if(customerService.deleteCustomerById(customerId) == true){
            response = CustomerResponse.<String>builder()
                    .message("Successfully deleted customer !!")
                    .success(true)
                    .build();
            return ResponseEntity.ok(response);
        }
        else{
            return ResponseEntity.notFound().build();
        }
    }


}
