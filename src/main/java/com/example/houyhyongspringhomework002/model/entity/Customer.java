package com.example.houyhyongspringhomework002.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Customer {
    private Integer customerId;
    private String customerName;
    private String customerAddress;
    private String customerPhone;

    //if dont use lombok we need generate constructor, setter, getter,...
}
