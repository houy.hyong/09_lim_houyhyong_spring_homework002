package com.example.houyhyongspringhomework002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Houyhyongspringhomework002Application {

	public static void main(String[] args) {
		SpringApplication.run(Houyhyongspringhomework002Application.class, args);
	}

}
