package com.example.houyhyongspringhomework002.repository;

import com.example.houyhyongspringhomework002.model.entity.Invoice;
import com.example.houyhyongspringhomework002.model.request.InvoiceRequest;
import org.apache.ibatis.annotations.*;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Mapper
public interface InvoiceRepository {


    // 1 - findAllInvoice
    @Select("SELECT * FROM invoice_tb ORDER BY invoice_id ASC")
    @Results(
            id = "InvoiceMapper",
            value = {
                    @Result(property = "invoiceId", column = "invoice_id"),
                    @Result(property = "invoiceDate", column = "invoice_date"),
                    //customer and invoice 1:M relationship __ annotation @One then select customer's info from path getAllCustomerById in CustomerRepository
                    @Result(property = "customer", column = "customer_id",
                        one = @One(select = "com.example.houyhyongspringhomework002.repository.CustomerRepository.getAllCustomerById")
                    ),
                    @Result(property = "products", column = "invoice_id",
                        many = @Many(select = "com.example.houyhyongspringhomework002.repository.ProductRepository.getProductByInvoiceId")
                    )
            }
    )
    List<Invoice> findAllInvoice();




    // 2 - Get invoice by ID
    @Select("SELECT * FROM invoice_tb WHERE invoice_id = #{bookId}")
    @ResultMap("InvoiceMapper")
    Invoice findInvoiceById(Integer invoiceId);



    // 3 - Add new invoice
    @Select("INSERT INTO invoice_tb (invoice_date, customer_id) " +
            "VALUES ( #{request.invoiceDate}, #{request.customerId} ) " +
            "RETURNING invoice_id")
    Integer addInvoice(@Param("request") InvoiceRequest invoiceRequest);

    @Select("INSERT INTO invoice_detail_tb (invoice_id, product_id) " +
            "VALUES ( #{invoiceId}, #{productId} ) ")
    Integer saveProductByInvoiceId(Integer invoiceId, Integer productId);



    // 4 - Update invoice by id
    @Select("UPDATE invoice_tb " +
            "SET invoice_date = #{request.invoiceDate}, " +
                "customer_id = #{request.customerId} " +
            "WHERE invoice_id = #{invoiceId} " +
            "RETURNING invoice_id" )
    Integer updateInvoice(@Param("request") InvoiceRequest invoiceRequest, Integer invoiceId);

    @Delete("DELETE FROM invoice_detail_tb WHERE invoice_id = #{invoiceId}")
    Integer deleteIdtByInvoiceId(Integer invoiceId);




    // 5 - Delete invoice by id
    @Delete("DELETE FROM invoice_tb WHERE invoice_id = #{invoiceId}")
    @ResultMap("InvoiceMapper")
    Boolean deleteInvoiceId(Integer invoiceId);

}
