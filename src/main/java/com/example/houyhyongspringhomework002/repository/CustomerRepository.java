package com.example.houyhyongspringhomework002.repository;

import com.example.houyhyongspringhomework002.model.entity.Customer;
import com.example.houyhyongspringhomework002.model.request.CustomerRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
public interface CustomerRepository {

    // 1- Get all customers
    @Select("SELECT * FROM customer_tb ORDER BY customer_id ASC")
    @Results(
            id = "customerMapper",
            value = {
                    @Result(property = "customerId", column = "customer_id"),
                    @Result(property = "customerName", column = "customer_name"),
                    @Result(property = "customerAddress", column = "customer_address"),
                    @Result(property = "customerPhone", column = "customer_phone")
            }
    )
    List<Customer> findAllCustomer();



    // 2- Get Customer by ID
    @Select("SELECT * FROM customer_tb WHERE customer_id = #{customerId}")
    @ResultMap("customerMapper")
    Customer getAllCustomerById(Integer customerId);



    // 3- Add new customer
    @Select("INSERT INTO customer_tb (customer_name, customer_address, customer_phone) " +
            "VALUES( #{request.customerName}, " +
                    "#{request.customerAddress}, " +
                    "#{request.customerPhone} ) " +
            "RETURNING customer_id;" )
    Integer addCustomer(@Param("request") CustomerRequest customerRequest);



    // 4- Update customer by id
    @Select("UPDATE customer_tb " +
            "SET customer_name = #{request.customerName}, " +
                "customer_address = #{request.customerAddress}, " +
                "customer_phone = #{request.customerPhone} " +
            "WHERE customer_id = #{customerId} " +
            "RETURNING customer_id")
    Integer updateCustomer(@Param("request") CustomerRequest customerRequest, Integer customerId);



    // 5- Delete customer by id
    @Delete("DELETE FROM customer_tb WHERE customer_id = #{customerId}")
    @ResultMap("customerMapper")
    boolean deleteCustomerById(Integer customerId);

}
