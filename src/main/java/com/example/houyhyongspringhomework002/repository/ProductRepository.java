package com.example.houyhyongspringhomework002.repository;

import com.example.houyhyongspringhomework002.model.entity.Customer;
import com.example.houyhyongspringhomework002.model.entity.Product;
import com.example.houyhyongspringhomework002.model.request.CustomerRequest;
import com.example.houyhyongspringhomework002.model.request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {

    // 1- Get All Products
    @Select("SELECT * FROM product_tb ORDER BY product_id ASC")
    @Results(
            id = "ProductMapper",
            value = {
                   @Result(property = "productId", column = "product_id"),
                   @Result(property = "productName", column = "product_name"),
                   @Result(property = "productPrice", column = "product_price")
            }
    )
    List<Product> findAllProduct();



    // 2- Get product by ID
    @Select("SELECT * FROM product_tb WHERE product_id = #{productId}")
    @ResultMap("ProductMapper")
    Product getAllProductById(Integer productId);



    // 3- Add new product
    @Select("INSERT INTO product_tb (product_name, product_price) " +
            "VALUES ( " +
                "#{request.productName}, " +
                "#{request.productPrice} ) " +
            "RETURNING product_id;")
    Integer addProduct(@Param("request") ProductRequest productRequest);



    // 4- Update product by ID
    @Select("UPDATE product_tb " +
            "SET product_name = #{request.productName}, " +
                "product_price = #{request.productPrice} " +
            "WHERE product_id = #{productId} " +
            "RETURNING product_id")
    Integer updateProduct(@Param("request") ProductRequest productRequest, Integer productId);



    // 5 - Delete product by ID
    @Delete("DELETE FROM product_tb WHERE product_id = #{productId}")
    @ResultMap("ProductMapper")
    boolean deleteProductById(Integer productId);


    // 6 - getProductByInvoiceId
    @Select("SELECT prod.product_id, prod.product_name, prod.product_price " +
            "FROM product_tb prod " +
            "INNER JOIN invoice_detail_tb inv_det " +
            "ON prod.product_id = inv_det.product_id " +
            "WHERE inv_det.invoice_id = #{invoiceId}")
    @ResultMap("ProductMapper")
    List<Product> getProductByInvoiceId(Integer invoiceId);

}
